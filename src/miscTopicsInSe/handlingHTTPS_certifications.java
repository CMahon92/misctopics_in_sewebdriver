package miscTopicsInSe;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.github.bonigarcia.wdm.WebDriverManager;

public class handlingHTTPS_certifications {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
		//Creates new ChromeOptions Class
		ChromeOptions chrome = new ChromeOptions();
		
		//accepts insecure SSL certifications
		chrome.setAcceptInsecureCerts(true);
		

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(chrome);
		driver.get("https://expired.badssl.com/");
		
		System.out.println(driver.getTitle());

	
	}

}
